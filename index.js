const backdraft = require('backdraft-js')
const diff = require('diff')

const t = {
  "{entityMap": {
    "0": {
      "type": "DOCUMENT_LINK",
      "mutability": "IMMUTABLE",
      "data": {
        "item": {
          "id": -599,
          "elementId": -499,
          "name": "Adult_FP_-_New_Patient_P...pdf"
        }
      }
    }
  },
  "blocks": [{
    "key": "bpe31",
    "text": "jhdrbgjk brjk dhrg",
    "type": "unstyled",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [],
    "data": {}
  }, {
    "key": "1as5o",
    "text": "Adult_FP_-_New_Patient_P...pdf jfbdhrb kgsb ",
    "type": "unstyled",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [{
      "offset": 0,
      "length": 30,
      "key": 0
    }],
    "data": {}
  }, {
    "key": "a5g0s",
    "text": "drg drhbg kjdhb gjkhdrsb gh",
    "type": "unstyled",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [],
    "data": {}
  }]
}
const t2 = {
  "entityMap": {
    "0": {
      "type": "DOCUMENT_LINK",
      "mutability": "IMMUTABLE",
      "data": {
        "item": {
          "id": -596,
          "elementId": -496,
          "name": "Medicare_Follow_up_Well...docx"
        }
      }
    }
  },
  "blocks": [{
    "key": "bpe31",
    "text": "jhdrbgjk brjk dhrg",
    "type": "unstyled",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [],
    "data": {}
  }, {
    "key": "1as5o",
    "text": "ghgv Medicare_Follow_up_Well...docx jfbdhrb kgsb ",
    "type": "unstyled",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [{
      "offset": 5,
      "length": 30,
      "key": 0
    }],
    "data": {}
  }, {
    "key": "a5g0s",
    "text": "drg drhbg kjdhb gjkhdrsb gh",
    "type": "unstyled",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [],
    "data": {}
  }, {
    "key": "6icdf",
    "text": "sef sefs",
    "type": "ordered-list-item",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [],
    "data": {}
  }, {
    "key": "nruh",
    "text": "sfsefsefesfsf",
    "type": "ordered-list-item",
    "depth": 0,
    "inlineStyleRanges": [],
    "entityRanges": [],
    "data": {}
  }]
}
// console.log(diff. diff.main('text 1 ', '  text 2'))
console.log(
  diff.createPatch(
    'diff',
    backdraft(t).join('\n'),
    backdraft(t2).join('\n'),
    'old file',
    'new file'
  )
)
